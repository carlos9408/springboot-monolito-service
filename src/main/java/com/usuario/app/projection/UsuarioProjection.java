package com.usuario.app.projection;

import java.util.Set;

public interface UsuarioProjection {

	String getNombre();

	Set<CredencialesUsuarioProjection> getCredencialesUsuario();

	public interface CredencialesUsuarioProjection {
		
		String getUsername();

		String getPassword();
	}
}
