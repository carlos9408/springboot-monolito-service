package com.usuario.app.projection;

public interface CredencialesProjection {

	String getUsername();

	String getPassword();
}
