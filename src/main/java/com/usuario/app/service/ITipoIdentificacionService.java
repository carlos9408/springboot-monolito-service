package com.usuario.app.service;

import java.util.List;

import com.usuario.app.entity.TipoIdentificacion;
import com.usuario.app.entity.Usuario;

public interface ITipoIdentificacionService {

	List<TipoIdentificacion> getAll();

	List<Usuario> getByTipoIdentificacion(final Long id);

}
