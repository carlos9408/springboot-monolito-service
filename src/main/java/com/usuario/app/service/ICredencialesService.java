package com.usuario.app.service;

import java.util.List;

import com.usuario.app.dto.rq.CredencialesUpdateRQ;
import com.usuario.app.dto.rs.CredencialesUsuarioRS;
import com.usuario.app.entity.CredencialesUsuario;
import com.usuario.app.entity.Usuario;
import com.usuario.app.projection.CredencialesProjection;

public interface ICredencialesService {

	List<Usuario> getCredenciales(final Long idUsuario);

	CredencialesUsuarioRS getCredencialesDTO(final Long idUsuario);

	List<CredencialesProjection> getCredencialesProjection(final Long idUsuario);

	CredencialesUsuario update(final CredencialesUpdateRQ rq);
}
