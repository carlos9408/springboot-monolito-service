package com.usuario.app.service;

import java.util.List;

import com.usuario.app.entity.EntidadComun;

public interface IGenericService {

	<T extends EntidadComun> T findById(final Long id, final Class<T> clase);

	<T extends EntidadComun> List<T> getAll(final Class<T> clase);

	<T extends EntidadComun> List<T> findByEstado(final boolean estado, final Class<T> clase);
}
