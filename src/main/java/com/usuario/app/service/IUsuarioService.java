package com.usuario.app.service;

import java.util.List;

import com.usuario.app.dto.rq.UsuarioRQ;
import com.usuario.app.dto.rs.UsuarioRS;
import com.usuario.app.entity.Usuario;
import com.usuario.app.projection.UsuarioProjection;

public interface IUsuarioService {

	List<Usuario> getAll();

	Usuario findById(final Long id);

	UsuarioProjection getCredenciales(final Long id);

	UsuarioRS save(final UsuarioRQ rq);

	void update(final Long id, final String nombre);

	void delete(final Long id);

	Usuario setEstado(final Long id, final Boolean estado);
}
