package com.usuario.app.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import com.usuario.app.entity.EntidadComun;
import com.usuario.app.service.IGenericService;
import com.usuario.app.util.Util;

@Service
@Transactional
public class IGenericServiceImpl implements IGenericService {

	@Autowired
	private EntityManager em;

	private static final Reflections REFLECTIONS = new Reflections(Util.ENTITY_PACKAGE, new SubTypesScanner(false));

	@Override
	public <T extends EntidadComun> T findById(final Long id, final Class<T> type) {
		if (entidadExiste(type.getSimpleName())) {
			try {
				final CriteriaBuilder cb = em.getCriteriaBuilder();
				final CriteriaQuery<T> cr = cb.createQuery(type);
				final Root<T> root = cr.from(type);
				cr.select(root).where(cb.and(cb.equal(root.get("id"), id), cb.equal(root.get("estado"), true)));
				return em.createQuery(cr).getSingleResult();
			} catch (NoResultException e) {
				throw new ResponseStatusException(HttpStatus.OK, Util.NO_RESULTS_FOUND);
			}
		}
		throw new ResponseStatusException(HttpStatus.OK, Util.NO_RESULTS_FOUND);

	}

	@Override
	public <T extends EntidadComun> List<T> getAll(Class<T> clase) {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	public <T extends EntidadComun> List<T> findByEstado(boolean estado, Class<T> clase) {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	private boolean entidadExiste(final String entityName) {
		return REFLECTIONS.getSubTypesOf(EntidadComun.class).stream().map(foo -> foo.getSimpleName())
				.collect(Collectors.toList()).stream().anyMatch(entity -> entityName.equalsIgnoreCase(entity));
	}

}
