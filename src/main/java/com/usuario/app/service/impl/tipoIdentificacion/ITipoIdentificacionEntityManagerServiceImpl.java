package com.usuario.app.service.impl.tipoIdentificacion;

import java.util.List;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usuario.app.entity.TipoIdentificacion;
import com.usuario.app.entity.Usuario;
import com.usuario.app.repository.TipoIdentificacionRepository;
import com.usuario.app.service.ITipoIdentificacionService;

@Service
@Transactional
public class ITipoIdentificacionEntityManagerServiceImpl implements ITipoIdentificacionService {

	@Autowired
	private TipoIdentificacionRepository tipoIdentificacionRepo;

	@Autowired
	private EntityManager em;

	@Override
	@Transactional(readOnly = true)
	public List<TipoIdentificacion> getAll() {
		return tipoIdentificacionRepo.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> getByTipoIdentificacion(final Long id) {
		EntityGraph<Usuario> graph = em.createEntityGraph(Usuario.class);
		graph.addSubgraph("tipoIdentificacion", TipoIdentificacion.class);

		return em.createQuery("SELECT u FROM Usuario u JOIN u.tipoIdentificacion ti WHERE ti.id =:id ORDER BY u.nombre", Usuario.class)
				.setParameter("id", id)
				.setHint("javax.persistence.loadgraph", graph)
				.getResultList();
	}
}
