package com.usuario.app.service.impl.usuario;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import com.usuario.app.dto.rq.UsuarioRQ;
import com.usuario.app.dto.rs.UsuarioRS;
import com.usuario.app.entity.CredencialesUsuario;
import com.usuario.app.entity.TipoIdentificacion;
import com.usuario.app.entity.Usuario;
import com.usuario.app.projection.UsuarioProjection;
import com.usuario.app.repository.CredencialesUsuarioRepository;
import com.usuario.app.repository.TipoIdentificacionRepository;
import com.usuario.app.repository.UsuarioRepository;
import com.usuario.app.service.IUsuarioService;
import com.usuario.app.util.Util;

@Service
@Transactional
@Primary
public class IUsuarioServiceRepoImpl implements IUsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepo;

	@Autowired
	private TipoIdentificacionRepository tipoIdentificacionRepo;

	@Autowired
	private CredencialesUsuarioRepository credencialesRepo;

	@Autowired
	private EntityManager em;

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> getAll() {
		return usuarioRepo.getAllRepo();
	}

	@Override
	public UsuarioProjection getCredenciales(final Long id) {
		final Optional<UsuarioProjection> usuario = usuarioRepo.getCredenciales(id);
		if (usuario.isPresent()) {
			return usuario.get();
		}
		throw new ResponseStatusException(HttpStatus.OK, Util.NO_RESULTS_FOUND);
	}

	@Override
	@Transactional
	public UsuarioRS save(final UsuarioRQ rq) {
		final Optional<TipoIdentificacion> ti = tipoIdentificacionRepo.findByCodigo(rq.getCodigoTipoIdentificacion());
		if (ti.isPresent()) {
			final Usuario u = usuarioRepo.save(new Usuario(rq.getNombre(), ti.get()));
			return UsuarioRS.builder().id(u.getId()).nombre(u.getNombre())
					.tipoIdentificacion(u.getTipoIdentificacion().getCodigo()).build();
		}
		throw new ResponseStatusException(HttpStatus.OK,
				new StringBuilder(Util.SAVE_ERROR).append(Usuario.class.getSimpleName()).toString());
	}

	@Override
	@Transactional
	public void update(final Long id, final String nombre) {
		final Optional<Usuario> u = usuarioRepo.findById(id);
		if (u.isPresent()) {
			usuarioRepo.update(id, nombre);
			return;
		}
		throw new ResponseStatusException(HttpStatus.OK,
				new StringBuilder(Util.UPDATE_ERROR).append(Usuario.class.getSimpleName()).toString());
	}

	@Override
	@Transactional
	public void delete(final Long id) {
		Optional<Usuario> u = usuarioRepo.findById(id);
		if (u.isPresent()) {
			List<CredencialesUsuario> credenciales = credencialesRepo.findByUsuario(u.get());
			credencialesRepo.deleteAll(credenciales);
			usuarioRepo.delete(u.get());
			return;
		}
		throw new ResponseStatusException(HttpStatus.OK, Util.NO_RESULTS_FOUND);
	}

	@Override
	public Usuario setEstado(final Long id, final Boolean estado) {
		final Optional<Usuario> u = usuarioRepo.findById(id);
		if (u.isPresent()) {
			u.get().setEstado(estado);
			return em.merge(u.get());
		}
		throw new ResponseStatusException(HttpStatus.OK,
				new StringBuilder(Util.UPDATE_ERROR).append(Usuario.class.getSimpleName()).toString());
	}

	@Override
	public Usuario findById(final Long id) {
		final Optional<Usuario> u = usuarioRepo.findById(id);
		if (u.isPresent()) {
			return u.get();
		}
		throw new ResponseStatusException(HttpStatus.OK, Util.NO_RESULTS_FOUND);
	}
}
