package com.usuario.app.service.impl.usuario;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usuario.app.dto.rq.UsuarioRQ;
import com.usuario.app.dto.rs.UsuarioRS;
import com.usuario.app.entity.Usuario;
import com.usuario.app.projection.UsuarioProjection;
import com.usuario.app.service.IUsuarioService;

@Service
@Transactional
public class IUsuarioServiceEntityManagerImpl implements IUsuarioService {

	@Autowired
	private EntityManager em;

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> getAll() {
		return em.createQuery("SELECT u FROM Usuario u", Usuario.class).getResultList();
	}

	@Override
	public Usuario findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UsuarioProjection getCredenciales(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UsuarioRS save(UsuarioRQ rq) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Long id, String nombre) {
		// TODO Auto-generated method stub

	}

	public void delete(Long id) {
	};

	@Override
	public Usuario setEstado(Long id, Boolean estado) {
		return null;
	}

}
