package com.usuario.app.service.impl.tipoIdentificacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usuario.app.entity.TipoIdentificacion;
import com.usuario.app.entity.Usuario;
import com.usuario.app.repository.TipoIdentificacionRepository;
import com.usuario.app.service.ITipoIdentificacionService;

@Service
@Transactional
@Primary
public class ITipoIdentificacionServiceImpl implements ITipoIdentificacionService {

	@Autowired
	private TipoIdentificacionRepository tipoIdentificacionRepo;

	@Override
	@Transactional(readOnly = true)
	public List<TipoIdentificacion> getAll() {
		return tipoIdentificacionRepo.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> getByTipoIdentificacion(final Long id) {
		return tipoIdentificacionRepo.getByTipoIdentificacion(id);
	}
}
