package com.usuario.app.service.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import com.usuario.app.dto.rq.CredencialesUpdateRQ;
import com.usuario.app.dto.rs.CredencialesRS;
import com.usuario.app.dto.rs.CredencialesUsuarioRS;
import com.usuario.app.entity.CredencialesUsuario;
import com.usuario.app.entity.Usuario;
import com.usuario.app.projection.CredencialesProjection;
import com.usuario.app.repository.CredencialesUsuarioRepository;
import com.usuario.app.repository.UsuarioRepository;
import com.usuario.app.service.ICredencialesService;
import com.usuario.app.util.Util;

@Service
@Transactional
public class ICredencialesServiceImpl implements ICredencialesService {

	@Autowired
	private EntityManager em;

	@Autowired
	private CredencialesUsuarioRepository credencialesRepo;

	@Autowired
	private UsuarioRepository usuarioRepo;

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> getCredenciales(final Long id) {
		EntityGraph<Usuario> graph = em.createEntityGraph(Usuario.class);
		graph.addSubgraph("credencialesUsuario", CredencialesUsuario.class);

		return em.createQuery(
				"SELECT u FROM Usuario u LEFT JOIN u.credencialesUsuario cu WHERE cu.usuario.id =:id ORDER BY u.nombre",
				Usuario.class).setParameter("id", id).setHint(Util.LOAD_GRAPH, graph).getResultList();
	}

	@Override
	@Transactional(readOnly = true)
	public CredencialesUsuarioRS getCredencialesDTO(final Long idUsuario) {
		List<CredencialesRS> credenciales = credencialesRepo.getCredencialesDTO(idUsuario);

		if (credenciales.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.OK, Util.NO_RESULTS_FOUND);
		}

		final String nombreUsuario = credenciales.get(Util.ZERO).getNombreUsuario();
		final String tipoIdentificacion = credenciales.get(Util.ZERO).getTipoIdentificacion();

		// TODO Cambiar estructura
		final List<Map<String, String>> credencialesUsuario = credenciales.stream().map(foo -> {
			return Util.createCredenciales(foo);
		}).collect(Collectors.toList());

		return CredencialesUsuarioRS.builder().idUsuario(idUsuario).nombreUsuario(nombreUsuario)
				.tipoIdentificacion(tipoIdentificacion).credenciales(credencialesUsuario).build();

	}

	@Override
	@Transactional(readOnly = true)
	public List<CredencialesProjection> getCredencialesProjection(final Long idUsuario) {
		return credencialesRepo.getCredencialesProjection(idUsuario);
	}

	@Override
	public CredencialesUsuario update(final CredencialesUpdateRQ rq) {
		Usuario u = usuarioRepo.findById(rq.getId()).orElse(null);
		if (null != u) {
			CredencialesUsuario credencial = credencialesRepo.findByUsername(rq.getUsername()).orElse(null);
			if (credencial != null) {
				credencial.setPassword(rq.getPassword());
				em.merge(credencial);
				return credencial;
			} else {
				throw new ResponseStatusException(HttpStatus.OK,
						new StringBuilder(Util.NO_CREDENCIAL_FOUND).append(" ").append(rq.getUsername()).toString());
			}
		}
		throw new ResponseStatusException(HttpStatus.OK, Util.NO_RESULTS_FOUND);
	}
}
