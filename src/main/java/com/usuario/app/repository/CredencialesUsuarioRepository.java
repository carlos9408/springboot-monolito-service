package com.usuario.app.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.usuario.app.dto.rs.CredencialesRS;
import com.usuario.app.entity.CredencialesUsuario;
import com.usuario.app.entity.Usuario;
import com.usuario.app.projection.CredencialesProjection;
import com.usuario.app.util.Util;

@Repository
public interface CredencialesUsuarioRepository extends CrudRepository<CredencialesUsuario, Long> {

	@Query(value = "SELECT " + Util.DTO_PACKAGE + "CredencialesRS(u.id, u.nombre, ti.codigo, cu.username, cu.password)"
			+ " FROM CredencialesUsuario cu JOIN cu.usuario u JOIN u.tipoIdentificacion ti WHERE u.id=:idUsuario ORDER BY u.nombre")
	List<CredencialesRS> getCredencialesDTO(@Param("idUsuario") final Long idUsuario);

	@Query(value = "SELECT cu.username AS username, cu.password AS password FROM CredencialesUsuario cu JOIN cu.usuario u JOIN u.tipoIdentificacion ti WHERE u.id=:idUsuario ORDER BY u.nombre")
	List<CredencialesProjection> getCredencialesProjection(@Param("idUsuario") final Long idUsuario);

	Optional<CredencialesUsuario> findByUsername(final String username);

	List<CredencialesUsuario> findByUsuario(final Usuario usuario);

}
