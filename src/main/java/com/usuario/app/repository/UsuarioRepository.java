package com.usuario.app.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.usuario.app.entity.Usuario;
import com.usuario.app.projection.UsuarioProjection;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	@Query(value = "SELECT u FROM Usuario u")
	List<Usuario> getAllRepo();

	@Query(value = "SELECT u FROM Usuario u JOIN u.credencialesUsuario cu WHERE u.id=:id")
	Optional<UsuarioProjection> getCredenciales(@Param("id") final Long id);

	@Query(value = "SELECT * FROM USUARIO", nativeQuery = true)
	List<Usuario> getAllNativo();

	@Modifying
	@Query("UPDATE Usuario u SET u.nombre =:nombre WHERE u.id=:id")
	void update(@Param("id") final Long id, @Param("nombre") final String nombre);

}
