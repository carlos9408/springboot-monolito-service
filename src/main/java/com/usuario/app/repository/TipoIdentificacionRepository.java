package com.usuario.app.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.usuario.app.entity.TipoIdentificacion;
import com.usuario.app.entity.Usuario;

@Repository
public interface TipoIdentificacionRepository extends JpaRepository<TipoIdentificacion, Long> {

	@Query(value = "SELECT u FROM Usuario u JOIN FETCH u.tipoIdentificacion ti WHERE ti.id =:id ORDER BY u.nombre")
	List<Usuario> getByTipoIdentificacion(@Param("id") final Long id);

	Optional<TipoIdentificacion> findByCodigo(final String codigo);

}
