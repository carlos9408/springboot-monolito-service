package com.usuario.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootUsuarioServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootUsuarioServiceApplication.class, args);
	}

}
