package com.usuario.app.dto.rq;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
public class UsuarioRQ implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nombre;

	private String codigoTipoIdentificacion;

}
