package com.usuario.app.dto.rq;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
public class CredencialesUpdateRQ implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String username;

	private String password;

	public CredencialesUpdateRQ() {
		// Default constructor
	}
}
