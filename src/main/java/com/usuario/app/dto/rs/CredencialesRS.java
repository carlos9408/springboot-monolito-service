package com.usuario.app.dto.rs;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
public class CredencialesRS implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idUsuario;

	private String nombreUsuario;

	private String tipoIdentificacion;

	private String username;

	private String password;

}
