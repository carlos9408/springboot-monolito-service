package com.usuario.app.dto.rs;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
public class UsuarioRS implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String nombre;

	private String tipoIdentificacion;

}
