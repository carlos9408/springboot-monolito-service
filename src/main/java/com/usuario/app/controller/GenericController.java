package com.usuario.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usuario.app.entity.EntidadComun;
import com.usuario.app.service.IGenericService;

@RestController
@RequestMapping("/generic")
public class GenericController {

	@Autowired
	private IGenericService genericSerice;

	@GetMapping("/all")
	public <T extends EntidadComun> List<T> getAll() {
		return genericSerice.getAll(null);
	}

	@GetMapping("/find/{id}")
	public <T extends EntidadComun> T findById() {
		// TODO genericService.algo
		return genericSerice.findById(null, null);
	}

	@GetMapping("/getByEstado")
	public <T extends EntidadComun> List<T> findByEstado() {
		// TODO genericService.algo
		return genericSerice.findByEstado(false, null);
	}
}