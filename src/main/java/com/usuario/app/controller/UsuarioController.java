package com.usuario.app.controller;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usuario.app.dto.rq.UsuarioRQ;
import com.usuario.app.dto.rs.UsuarioRS;
import com.usuario.app.entity.Usuario;
import com.usuario.app.projection.UsuarioProjection;
import com.usuario.app.service.IGenericService;
import com.usuario.app.service.ITipoIdentificacionService;
import com.usuario.app.service.IUsuarioService;
import com.usuario.app.util.Util;

@RestController
@RequestMapping("/user")
public class UsuarioController {

	@Autowired
	private IUsuarioService usuarioService;

	@Autowired
	private ITipoIdentificacionService tipoIdentificacionService;

	@Autowired
	private IGenericService genericSerice;

	@GetMapping("/all")
	public List<Usuario> getAll() {
		return usuarioService.getAll();
	}

	@GetMapping("/find/{id}")
	public Usuario findById(@PathVariable(name = "id") final Long id) {
		return usuarioService.findById(id);
	}

	@GetMapping("/generic/find/{id}")
	public Usuario findGenericById(@PathVariable(name = "id") final Long id) {
		return genericSerice.findById(id, Usuario.class);
	}

	@GetMapping("/by/tipoId/{id}")
	public List<Usuario> getByTipoIdentificacion(@PathVariable(name = "id") final Long id) {
		return tipoIdentificacionService.getByTipoIdentificacion(id);
	}

	@GetMapping("/credenciales/{id}")
	public UsuarioProjection getCredenciales(@PathVariable(name = "id") final Long id) {
		return usuarioService.getCredenciales(id);
	}

	@PostMapping("/save")
	public UsuarioRS save(@RequestBody final UsuarioRQ rq) {
		return usuarioService.save(rq);
	}

	@PostMapping("/{id}/update")
	public ResponseEntity<?> update(@PathVariable(name = "id") final Long id, @RequestBody final String rq) {
		final String nombre = new JSONObject(rq).getString("nombre");
		usuarioService.update(id, nombre);
		return ResponseEntity.status(HttpStatus.OK).body(Util.UPDATE_SUCCESS);
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable(name = "id") final Long id) {
		usuarioService.delete(id);
		return ResponseEntity.status(HttpStatus.OK).body(Util.DELETE_SUCCESS);
	}

	@PutMapping("/{id}/estado/{estado}")
	public ResponseEntity<?> cambiarEstado(@PathVariable(name = "id") final Long id,
			@PathVariable(name = "estado") final Boolean estado) {
		final Usuario u = usuarioService.setEstado(id, estado);
		return ResponseEntity.status(HttpStatus.OK).body(u);
	}
}