package com.usuario.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usuario.app.entity.TipoIdentificacion;
import com.usuario.app.service.ITipoIdentificacionService;

@RestController
@RequestMapping("/tipoId")
public class TipoIdentificacionController {

	@Autowired
	private ITipoIdentificacionService tipoIdentificacionService;

	@GetMapping("/all")
	public List<TipoIdentificacion> getAll() {
		return tipoIdentificacionService.getAll();
	}
}