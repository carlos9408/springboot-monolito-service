package com.usuario.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usuario.app.dto.rq.CredencialesUpdateRQ;
import com.usuario.app.dto.rs.CredencialesUsuarioRS;
import com.usuario.app.entity.CredencialesUsuario;
import com.usuario.app.entity.Usuario;
import com.usuario.app.projection.CredencialesProjection;
import com.usuario.app.service.ICredencialesService;

@RestController
@RequestMapping("/credenciales")
public class CredencialesUsuarioController {

	@Autowired
	private ICredencialesService credencialesService;

	@GetMapping("/{idUsuario}")
	public List<Usuario> getCredencialesByIdUsuario(@PathVariable(name = "idUsuario") final Long id) {
		return credencialesService.getCredenciales(id);
	}

	@GetMapping("/dto/{idUsuario}")
	public CredencialesUsuarioRS getCredencialesDTOByIdUsuario(@PathVariable(name = "idUsuario") final Long id) {
		return credencialesService.getCredencialesDTO(id);
	}

	@GetMapping("/projection/{idUsuario}")
	public List<CredencialesProjection> getCredencialesProjectionByIdUsuario(
			@PathVariable(name = "idUsuario") final Long id) {
		return credencialesService.getCredencialesProjection(id);
	}

	@PutMapping("/put/update")
	public CredencialesUsuario putUpdate(@RequestBody final CredencialesUpdateRQ rq) {
		return credencialesService.update(rq);
	}

	@PostMapping("/post/update")
	public CredencialesUsuario postUpdate(@RequestBody final CredencialesUpdateRQ rq) {
		return credencialesService.update(rq);
	}
}